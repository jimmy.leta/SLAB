program main
  integer           :: nargs
  character(len=32) :: input_file
  character(len=32) :: output_file
    
  nargs = command_argument_count()
  if (nargs /= 2) then
    write(*,*) "Usage: ./Slab <input file> <output file>"
  return
  end if

  call get_command_argument(1, input_file)
  call get_command_argument(2, output_file)
  call state(input_file, output_file)
  return
end program